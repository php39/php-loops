<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        p {
            border: 1px solid black;
            margin: 0
        }
    </style>
</head>
<body>
    <?php
        for($i=0; $i < 16; $i++) {
            if($i % 2 !== 0 ){
                for($j = 1; $j < 11; $j++) {
                    $ans = $i*$j;
                    echo "<p>$i x $j = $ans</p>";
                }
            }
        }
    ?>

</body>
</html>